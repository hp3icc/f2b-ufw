#!/bin/sh
if [[ $EUID -ne 0 ]]; then
	whiptail --title "F2B+UFW" --msgbox "Debe ejecutar este script como usuario ROOT" 0 50
	exit 0
fi


bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ufw)"

bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/f2b+ufw.sh)"

menu-ufw
